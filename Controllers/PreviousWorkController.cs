using UFRJobs.Models;
using UFRJobs.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace UFRJobs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreviousWorkController : ControllerBase
    {
        private readonly PreviousWorkService _previousWorkService;

        public PreviousWorkController(PreviousWorkService previousWorkService)
        {
            _previousWorkService = previousWorkService;
        }

        [HttpGet]
        public ActionResult<List<PreviousWork>> Get() =>
            _previousWorkService.Get();

        [HttpGet("{id:length(24)}", Name = "GetPreviousWork")]
        public ActionResult<PreviousWork> Get(string id)
        {
            var previousWork = _previousWorkService.Get(id);

            if (previousWork == null)
            {
                return NotFound();
            }
            return previousWork;
        }

        [HttpPost]
        public ActionResult<PreviousWork> Create(PreviousWork previousWork)
        {
            _previousWorkService.Create(previousWork);

            return CreatedAtRoute("PreviousWork", new { id = previousWork.Id.ToString() }, previousWork);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, PreviousWork previousWorkIn)
        {
            var previousWork = _previousWorkService.Get(id);

            if (previousWork == null)
            {
                return NotFound();
            }

            _previousWorkService.Update(id, previousWorkIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var previousWork = _previousWorkService.Get(id);

            if (previousWork == null)
            {
                return NotFound();
            }

            _previousWorkService.Remove(previousWork.Id);

            return NoContent();
        }
    }
}