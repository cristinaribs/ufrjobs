using UFRJobs.Models;
using UFRJobs.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace UFRJobs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AcademicInfoController : ControllerBase
    {
        private readonly AcademicInfoService _academicInfoService;

        public AcademicInfoController(AcademicInfoService academicInfoService)
        {
            _academicInfoService = academicInfoService;
        }

        [HttpGet]
        public ActionResult<List<AcademicInfo>> Get() =>
            _academicInfoService.Get();

        [HttpGet("{id:length(24)}", Name = "GetAcademicInfo")]
        public ActionResult<AcademicInfo> Get(string id)
        {
            var academicInfo = _academicInfoService.Get(id);

            if (academicInfo == null)
            {
                return NotFound();
            }
            return academicInfo;
        }

        [HttpPost]
        public ActionResult<AcademicInfo> Create(AcademicInfo academicInfo)
        {
            _academicInfoService.Create(academicInfo);

            return CreatedAtRoute("GetAcademicInfo", new { id = academicInfo.Id.ToString() }, academicInfo);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, AcademicInfo academicInfoIn)
        {
            var academicInfo = _academicInfoService.Get(id);

            if (academicInfo == null)
            {
                return NotFound();
            }

            _academicInfoService.Update(id, academicInfoIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var academicInfo = _academicInfoService.Get(id);

            if (academicInfo == null)
            {
                return NotFound();
            }

            _academicInfoService.Remove(academicInfo.Id);

            return NoContent();
        }
    }
}