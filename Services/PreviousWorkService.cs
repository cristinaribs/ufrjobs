using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using UFRJobs.Models;

namespace UFRJobs.Services
{
    public class PreviousWorkService
    {
        private readonly IMongoCollection<PreviousWork> _previousworks;

        public PreviousWorkService(IUFRJobsDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _previousworks = database.GetCollection<PreviousWork>("PreviousWorks");

        }

        public List<PreviousWork> Get() =>
            _previousworks.Find(previouswork => true).ToList();

        public PreviousWork Get(string id) =>
            _previousworks.Find<PreviousWork>(previouswork => previouswork.Id == id).FirstOrDefault();

        public PreviousWork Create(PreviousWork previouswork)
        {
            _previousworks.InsertOne(previouswork);
            return previouswork;
        }

        public void Update(string id, PreviousWork previousworkIn) =>
            _previousworks.ReplaceOne(previouswork => previouswork.Id == id, previousworkIn);

        public void Remove(PreviousWork previousworkIn) =>
            _previousworks.DeleteOne(previouswork => previouswork.Id == previousworkIn.Id);

        public void Remove(string id) =>
            _previousworks.DeleteOne(previouswork => previouswork.Id == id);
    }
}