using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using UFRJobs.Models;

namespace UFRJobs.Services
{
    public class AcademicInfoService
    {
        private readonly IMongoCollection<AcademicInfo> _academicinfos;

        public AcademicInfoService(IUFRJobsDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _academicinfos = database.GetCollection<AcademicInfo>("AcademicInfos");

        }

        public List<AcademicInfo> Get() =>
            _academicinfos.Find(academicinfo => true).ToList();

        public AcademicInfo Get(string id) =>
            _academicinfos.Find<AcademicInfo>(academicinfo => academicinfo.Id == id).FirstOrDefault();

        public AcademicInfo Create(AcademicInfo academicinfo)
        {
            _academicinfos.InsertOne(academicinfo);
            return academicinfo;
        }

        public void Update(string id, AcademicInfo academicinfoIn) =>
            _academicinfos.ReplaceOne(academicinfo => academicinfo.Id == id, academicinfoIn);

        public void Remove(AcademicInfo academicinfoIn) =>
            _academicinfos.DeleteOne(academicinfo => academicinfo.Id == academicinfoIn.Id);

        public void Remove(string id) =>
            _academicinfos.DeleteOne(academicinfo => academicinfo.Id == id);
    }
}