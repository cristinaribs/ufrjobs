// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
// using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
// using Microsoft.OpenApi.Models;
//using Microsoft.EntityFrameworkCore;
using UFRJobs.Models;
using UFRJobs.Services;

namespace UFRJobs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<UFRJobsDatabaseSettings>(
                Configuration.GetSection(nameof(UFRJobsDatabaseSettings)));
            
            services.AddSingleton<IUFRJobsDatabaseSettings>(sp => 
                sp.GetRequiredService<IOptions<UFRJobsDatabaseSettings>>().Value);

            services.AddSingleton<UserService>();
            services.AddSingleton<AcademicInfoService>();
            services.AddSingleton<PreviousWorkService>();
            
            services.AddControllers()
                .AddNewtonsoftJson(options => options.UseMemberCasing());

            // services.AddSwaggerGen(c =>
            // {
            //     c.SwaggerDoc("v1", new OpenApiInfo { Title = "UFRJobs", Version = "v1" });
            // });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // app.UseSwagger();
                // app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "UFRJobs v1"));
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
