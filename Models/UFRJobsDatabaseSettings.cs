//esse arquivo é o que cria o contexto do banco de dados, e informa pra ele quais tabelas tem la dentro.

namespace UFRJobs.Models
{
    public class UFRJobsDatabaseSettings : IUFRJobsDatabaseSettings
    {
        public string Users { get; set; }
        public string PreviousWorks { get; set; }
        public string AcademicInfos { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IUFRJobsDatabaseSettings
    {
        string Users { get; set; }
        string PreviousWorks { get; set; }
        string AcademicInfos { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

/*
A UFRJobsDatabaseSettings (classe) é usada para armazenar os valores de propriedade da propria classe no appsettings.json. Os nomes de propriedade JSON e C# são iguais para facilitar o processo de mapeamento.
*/