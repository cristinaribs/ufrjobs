using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace UFRJobs.Models
{
    [BsonIgnoreExtraElements]
    public class PreviousWork //trabalho anterior do candidato
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string CompanyName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Position { get; set; }
        public string JobDescription { get; set; }
    }
}