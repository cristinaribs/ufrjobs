/* Esse arquivo é responsável por criar uma tabela "AcademicInfo" no banco de dados principal que se chama Candidates. 
Essa tabela deve ser relacionada a um usuário, e aí vai conter as informações acadêmicas daquele usuário. 
*/
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace UFRJobs.Models
{
    [BsonIgnoreExtraElements]
    public class AcademicInfo
    {
        /*Toda entrada em um bd deve conter um Id, pra identificação fácil. A parte esquerda do código representa o tipo de variável que é o Id, e dentro das chaves, os métodos que elas suportam. Esse get e set são métodos HTTP utilizados em sistemas RESTful pra, literalmente, pegar, e refatorar dados. */
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string SchoolName { get; set; } 
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Type { get; set; }
        public bool IsComplete { get; set; }
    }
}